package com.lpb.dogg

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Color
import android.os.Build
import android.util.Log
import androidx.fragment.app.Fragment
import com.google.firebase.ml.common.modeldownload.FirebaseCloudModelSource
import com.google.firebase.ml.common.modeldownload.FirebaseLocalModelSource
import com.google.firebase.ml.common.modeldownload.FirebaseModelDownloadConditions
import com.google.firebase.ml.common.modeldownload.FirebaseModelManager
import com.google.firebase.ml.custom.*
import com.google.firebase.perf.metrics.AddTrace

class Infer(ctx: Fragment) {
    companion object {
        const val SUBJECT_IMG_WIDTH = 224
        const val SUBJECT_IMG_HEIGHT = 224
    }

    private val li = ctx as OnInferenceListener

    private lateinit var interpreter: FirebaseModelInterpreter
    private lateinit var ioOpts: FirebaseModelInputOutputOptions

    private val probs = arrayListOf<Pair<Int, Float>>()

    init {
        loadModel()
    }

    @AddTrace(name="inferTrace", enabled=true)
    fun infer(bmp: Bitmap) {
        val b = Bitmap.createScaledBitmap(bmp, SUBJECT_IMG_WIDTH, SUBJECT_IMG_HEIGHT, true)
        val bNum = 0
        val i = Array(1) { Array(SUBJECT_IMG_HEIGHT) { Array(SUBJECT_IMG_WIDTH) { FloatArray(3) } } }
        for (x in 0 until 224) {
            for (y in 0 until 224) {
                val px = b.getPixel(x, y)
                i[bNum][x][y][0] = (Color.red(px) - 127) / 255.0f
                i[bNum][x][y][1] = (Color.green(px) - 127) / 255.0f
                i[bNum][x][y][2] = (Color.blue(px) - 127) / 255.0f
            }
        }

        val inputs = FirebaseModelInputs.Builder()
            .add(i)
            .build()

        interpreter.run(inputs, ioOpts).addOnSuccessListener { res ->
            val o = res.getOutput<Array<FloatArray>>(0)
            val prob = o[0]
            setResults(prob)
        }
    }

    fun getResults(): ArrayList<Pair<Int, Float>>? {
        if (!probs.isEmpty()) {
            return probs
        } else {
            Log.e("ML_TAG", "infer() must be run first")
        }
        return null
    }

    fun getTopForNumber(i: Int): List<Pair<Int, Float>>? {
        if (!probs.isEmpty()) {
            return probs.slice(0 until i)
        } else {
            Log.e("ML_TAG", "infer() must be run first")
        }
        return null
    }

    private fun setResults(arr: FloatArray) {
        Log.i("ML_TAG", "Setting results")
        val sorter = arrayListOf<Pair<Int, Float>>()
        arr.forEachIndexed { index, fl -> sorter.add(Pair(index, fl)) }
        val sorted = sorter.sortedWith(compareByDescending { it.second })
        sorted.forEach { probs.add(it) }
        li.inferenceComplete()
    }

    private fun loadModel() {
        var conditionsBuilder =
            FirebaseModelDownloadConditions.Builder().requireWifi()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            conditionsBuilder = conditionsBuilder.requireCharging().requireDeviceIdle()
        }

        val conditions = conditionsBuilder.build()

        val cloudSrc = FirebaseCloudModelSource.Builder("nasnet")
            .enableModelUpdates(true)
            .setInitialDownloadConditions(conditions)
            .setUpdatesDownloadConditions(conditions)
            .build()

        FirebaseModelManager.getInstance().registerCloudModelSource(cloudSrc)

        val localSrc = FirebaseLocalModelSource.Builder("local_model")
            .setAssetFilePath("test_nasnet.tflite")
            .build()

        FirebaseModelManager.getInstance().registerLocalModelSource(localSrc)

        val opts = FirebaseModelOptions.Builder()
            .setCloudModelName("nasnet")
            .setLocalModelName("local_model")
            .build()

        interpreter = FirebaseModelInterpreter.getInstance(opts)!!

        ioOpts = FirebaseModelInputOutputOptions.Builder()
            .setInputFormat(0, FirebaseModelDataType.FLOAT32, intArrayOf(1, SUBJECT_IMG_HEIGHT, SUBJECT_IMG_WIDTH, 3))
            .setOutputFormat(0, FirebaseModelDataType.FLOAT32, intArrayOf(1, 120)).build()
    }

    interface OnInferenceListener {
        fun inferenceComplete()
    }
}