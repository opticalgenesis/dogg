package com.lpb.dogg.fragments

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import com.lpb.dogg.R

class MainFragment : Fragment() {

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            9001 -> {
                if (resultCode == Activity.RESULT_OK) {
                    if (data !== null) {
                        if (data.data != null) {
                            val args = Bundle()
                            args.putString("IMAGE_URI", data.dataString)
                            val cf = ConfirmFragment()
                            cf.arguments = args
                            requireActivity().supportFragmentManager.beginTransaction()
                                .replace(R.id.activity_main_fragment_container, cf)
                                .addToBackStack("main").commit()
                        }
                    }
                }
            }
            9002 -> chooseImg()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_main, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val select = view.findViewById<Button>(R.id.fragment_main_select_image)
        val take = view.findViewById<Button>(R.id.fragment_main_take_image)

        select.setOnClickListener { chooseImg() }
        take.setOnClickListener { Toast.makeText(activity, "TODO", Toast.LENGTH_LONG).show() }
    }

    private fun chooseImg() {
        if (checkPermissions()) {
            val i = Intent(Intent.ACTION_GET_CONTENT)
            i.type = "image/*"
            startActivityForResult(i, 9001)
        } else {
            requestPermission(Manifest.permission.READ_EXTERNAL_STORAGE, 9002)
        }
    }

    private fun checkPermissions(): Boolean {
        return ActivityCompat.checkSelfPermission(
            requireActivity(),
            Manifest.permission.READ_EXTERNAL_STORAGE
        ) == PackageManager.PERMISSION_GRANTED
    }

    private fun requestPermission(permissionString: String, requestCode: Int) {
        ActivityCompat.requestPermissions(requireActivity(), arrayOf(permissionString), requestCode)
    }
}