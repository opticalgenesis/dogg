package com.lpb.dogg.fragments

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import androidx.fragment.app.Fragment
import com.lpb.dogg.R

class ConfirmFragment : Fragment() {

    private lateinit var path: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments !== null) {
            path = arguments?.getString("IMAGE_URI")!!
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
            inflater.inflate(R.layout.fragment_confirm, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val confirm = view.findViewById<Button>(R.id.confirm_confirm)
        val cancel = view.findViewById<Button>(R.id.confirm_cancel)
        val i = view.findViewById<ImageView>(R.id.fragment_confirm_image)

        val b = MediaStore.Images.Media.getBitmap(requireActivity().contentResolver, Uri.parse(path))

        i.setImageBitmap(b)

        confirm.setOnClickListener {
            val res = Results()
            val args = Bundle()
            args.putString("BITMAP", path)
            res.arguments = args

            requireActivity().supportFragmentManager.beginTransaction()
                .replace(R.id.activity_main_fragment_container, res).commit()
        }
    }
    // TODO determine probability of dog actually being in image and display it w new model
}