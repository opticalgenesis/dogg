package com.lpb.dogg.fragments

import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.google.firebase.perf.metrics.AddTrace
import com.lpb.dogg.Infer
import com.lpb.dogg.R
import java.io.BufferedReader
import java.io.InputStreamReader

class Results : Fragment(), Infer.OnInferenceListener {

    private val interpreter: Infer by lazy { Infer(this) }

    private lateinit var disp: TextView

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.fragment_results, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        disp = view.findViewById(R.id.fragment_results_result)
        val u = arguments?.getString("BITMAP")
        if (u !== null) {
            interpreter.infer(Bitmap.createScaledBitmap(MediaStore.Images.Media.getBitmap(requireActivity().contentResolver, Uri.parse(u)), 224, 224, true))
        } else {
            Log.i("ML_tAG", "Bitmap is null")
        }
    }

    override fun inferenceComplete() {
        val topThree = interpreter.getTopForNumber(3)!!
        val r = BufferedReader(InputStreamReader(requireActivity().assets.open("retrained_labels.txt")))
        val labels = r.readLines() as MutableList<String>
        labels.forEachIndexed { index, s ->
            labels[index] = s.replace("_", " ")
            if (s == "pembroke" || s == "cardigan") {
                val sb = StringBuilder()
                sb.append(s).append(" corgi")
                labels[index] = sb.toString()
            }
        }
        Toast.makeText(
            requireContext(),
            "1. ${labels[topThree[0].first]}: ${(topThree[0].second * 100)}%\n2. ${labels[topThree[1].first]}: ${topThree[1].second * 100}%\n3. ${labels[topThree[2].first]}: ${topThree[2].second}",
            Toast.LENGTH_LONG
        ).show()
        if (topThree[0].second < 0.86) {
            disp.text = "I'm not confident in my guess"
        } else {
            disp.text = "I'm ${topThree[0].second * 100}% sure this is a ${labels[topThree[0].first]}"
        }
    }

}